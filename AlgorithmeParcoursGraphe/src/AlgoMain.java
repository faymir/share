import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.function.Predicate;

public class AlgoMain {
    public static void print(String str){
        System.out.println(str);
    }

    public static void process (Etat node,List<Etat> generatedStates){
        Predicate<Etat> predicate = e -> e.compareTo(node);
        boolean res = generatedStates.removeIf(predicate);
        node.evaluate();
        System.out.print("{" + res + "  (" + Etat.printArray(node.getState()) + " | " + node.getValue() + ")} ");
    }
    public static void printGen(List<Etat> l){
        int [] E = Etat.E;
        for (Etat e: l) {
            e.calculate(E);
            System.out.print("{" + Etat.printArray(e.getState()) + " | " + e.getValue() + "}     ");
        }
        print("");
    }

    public static Vector< List<Etat> > processAndPrint(List<Etat> generatedStates, List<Etat> notYetExploredNodes, List<Etat> alreadyExploredNodes){
        print("\n----------------------------------------------------------------------------------------------------------\n");

        System.out.print("GeneratedStates: ");
        printGen(generatedStates);
        print("");
        System.out.print("NotYetExploredNodes: ");
        for (Etat node: notYetExploredNodes) {
            process(node,generatedStates);
        }
        print("");
        print("");
        //System.out.print("\n                                      ||||        ");
        System.out.print("alreadyExploredNodes: ");
        for (Etat node: alreadyExploredNodes) {
            process(node,generatedStates);
        }
        print("");
        print("");
        System.out.print("New GeneratedStates: ");
        printGen(generatedStates);

        print("\n----------------------------------------------------------------------------------------------------------\n");

        return new Vector<>(Arrays.asList(generatedStates, notYetExploredNodes, alreadyExploredNodes));
    }

    public static Etat resolutionBFs(Etat s0, int goal){
        if (goal == 0)
            return new Etat();
        List<Etat> notYetExploredNodes = new ArrayList<>();
        notYetExploredNodes.add(s0);
        List<Etat> alreadyExploredNodes = new ArrayList<>();
        Etat goalNode = new Etat();
        boolean success = false;
        int i =0;
        List<Etat> generatedStates = new ArrayList<>();

        while (notYetExploredNodes.size() != 0 && !success){
            print("notYet: " + notYetExploredNodes.size() + " already: " + alreadyExploredNodes.size() + " generated: " + generatedStates.size() );
            Etat firstNode = notYetExploredNodes.get(0);
            generatedStates = firstNode.successorsOf();
            notYetExploredNodes.remove(firstNode);
            alreadyExploredNodes.add(firstNode);

            Vector< List<Etat> > v = processAndPrint(generatedStates,notYetExploredNodes,alreadyExploredNodes);
            generatedStates = v.get(0);
            notYetExploredNodes = v.get(1);
            alreadyExploredNodes = v.get(2);

            for (Etat S : generatedStates) {
                S.evaluate();
                if(S.getValue() == goal){
                    success = true;
                    goalNode = S;
                    break;
                }
                else{
                    notYetExploredNodes.add(S);
                }
            }

            i++;
        }
        print("\n\n" + i );

        return (notYetExploredNodes.size() == 0)? null: goalNode;
    }

    public static Etat resolutionDFs(Etat s0, int goal){
        if (goal == 0)
            return new Etat();
        List<Etat> notYetExploredNodes = new ArrayList<>();
        notYetExploredNodes.add(s0);
        List<Etat> alreadyExploredNodes = new ArrayList<>();
        Etat goalNode = new Etat();
        int i =0;
        List<Etat> generatedStates = new ArrayList<>();
        boolean success = false;

        while (notYetExploredNodes.size() != 0 && !success){
            print("notYet: " + notYetExploredNodes.size() + " already: " + alreadyExploredNodes.size() + " generated: " + generatedStates.size() );
            Etat firstNode = notYetExploredNodes.get(0);
            generatedStates = firstNode.successorsOf();
            notYetExploredNodes.remove(firstNode);
            firstNode.evaluate();
            if(firstNode.getValue() == goal){
                success = true;
                goalNode = firstNode;
            }
            else {
                alreadyExploredNodes.add(firstNode);

                Vector< List<Etat> > v = processAndPrint(generatedStates,notYetExploredNodes,alreadyExploredNodes);
                generatedStates = v.get(0);
                notYetExploredNodes = v.get(1);
                alreadyExploredNodes = v.get(2);

                notYetExploredNodes.addAll(0, generatedStates);
            }
            i++;
        }
        print("\n\n" + i );

        return (notYetExploredNodes.size() == 0)? null: goalNode;
    }


    public static void main(String args[]){
        Etat goalNode = resolutionDFs(new Etat(),44);
        System.out.println(goalNode);
    }
}
//5 12 14 9 10 6