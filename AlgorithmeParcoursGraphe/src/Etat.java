import java.util.ArrayList;
import java.util.List;

public class Etat{
    private int value;
    private int [] state;
    public static int [] E = {5,12,14,9,10,6};

    public Etat(){
        this.value = 0;
        this.state = new int[E.length];
    }
    public Etat(int [] state){
        this.value = 0;
        this.state = state;
    }
    public Etat(Etat c){
        value = c.value;
        this.state = c.state.clone();
    }

    public void calculate(int [] E){
        int i = 0;
        value = 0;
        //System.out.print(printArray(E) + "\t           " + printArray(state) + "\t      ");
        for (int a :
                E) {
            value+=a*state[i++];
        }
        //System.out.println(value);
    }

    public static String printArray(int[] anArray) {
        String str = "";
        for (int i = 0; i < anArray.length; i++) {
            if (i > 0) {
                str += ", ";
            }
            str+= anArray[i];
        }
        return str;
    }

    public String toString(){
        int [] tab = E;
        String str = "";
        int end = 0;
        for (int i = 0; i < state.length; i++) {
            if(tab[i]*state[i] > 0) {
                str += tab[i] + " -> ";
                end += tab[i];
            }
        }
        str+= "END (" + end + ")";
        return Etat.printArray(state) +
                "\n" + str;
    }

    public void setState(int[] state) {
        this.state = state;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int[] getState() {
        return state;
    }

    public int length(){
        return state.length;
    }

    public boolean compareTo(Etat e){

        for (int i = 0; i < state.length; i++) {
            if(state[i]!=e.state[i])
                return false;
        }

        return true;
    }

    public List<Etat> successorsOf(){
        List<Etat> successors = new ArrayList<>();
        for (int i = 0; i < this.length(); i++) {
            int [] tab = state.clone();
            if(tab[i]!=1) {
                tab[i] = 1;
                successors.add(new Etat(tab));
            }
        }
        return  successors;
    }

    public void evaluate(){
        this.calculate(Etat.E);
    }
}
