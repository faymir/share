<?php
    function erreur($var){
        echo '<pre class="red">' . $var . '</pre>';
    }

    function debug($var){
        echo '<pre class="red">' . print_r($var,true) . '</pre>';
    }

    function str_random($taille){
        $chars = "7894561230qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        return substr(str_shuffle(str_repeat($chars,60)),0,$taille);
    }