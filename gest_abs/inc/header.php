<?php
/**
 * Created by Faymir.
 * User: Faymir
 * Date: 20-May-17
 * Time: 23:01
 */
?>
<?php
if (session_status() == PHP_SESSION_NONE){
  session_start();
}
$nbr=0;
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Gestion des Absences</title>
      <link rel="shortcut icon" href="assets/images/tetouan.png">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
  </head>

  <body>



    <nav class="navbar navbar-default ">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php" role="button">Gestion d'absence</a>
            <?php if(isset($_SESSION['type']) && $_SESSION['type']=='admin'):?>
            <ul class="nav navbar-nav">
                <li class=""><a  href="admin.php"><i class="fa fa-home fa-lg"></i> Home</a></li>
                <li class=""><a href="../php/adminModifEtudiant.php"><i class="fa fa-wrench fa-lg"></i>  Modifier Etudiants <span class="sr-only">(current)</span></a></li>
                <li class=""><a href="../php/EspaceTeacher_PlusEtudiants_Affichage.php"><i class="fa fa-pencil-square-o fa-lg"></i> Liste d'absences<span class="sr-only">(current)</span></a></li>
                <li><a href="adminMessage.php"><i class="fa fa-send-o"></i> Envoyer Message</a></li>
            </ul>
            <?php endif; ?>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
              <?php if(!isset($_SESSION['auth'])):?>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dérouler<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                <li><a href="../php/register.php">S'inscrire</a></li>
                <li><a href="../php/login.php">Se connecter</a></li>
                      <!--li class="divider"></li-->
                  </ul>
              </li>
              <?php else: ?>
                  <?php
                  require_once "../inc/db.php";
                  require_once "../inc/functions.php";
                  $req = $pdo->prepare('SELECT COUNT(*) AS compteur FROM messages WHERE username = ? OR username = ?');
                  $req->execute([$_SESSION['auth']->username,$_SESSION['auth']->email]);
                  $result = $req->fetch();
                  $nbr = $result->compteur;
                  ?>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Bienvenue , <i class="fa fa-user"></i> <?= $_SESSION['auth']->username?> <?php if($nbr!=0):?><span class="badge"><?= $nbr ?></span><?php endif;?> <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                      <li><a href="../php/login.php"><i class="fa fa-fw fa-user"></i> Profil </a></li>
                      <li><a href="../php/message.php"><i class="fa fa-fw fa-envelope"></i> Messages</a></li>
                      <li><a href="../php/logout.php"><i class="fa fa-fw fa-power-off"></i> Déconnexion </a></li>
                      <!--li class="divider"></li-->
                  </ul>
                </li>
              <?php endif; ?>
              <?php $_SESSION['nbrMessage']=$nbr; if($nbr>0){$_SESSION['flash']['info']='Vous avez reçus des messages';} ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

   
    <div class="container">
     <?php if(isset($_SESSION['flash'])): ?>
        <?php foreach ($_SESSION['flash'] as $type => $message): ?>
          <div class="alert alert-dismissible center-block alert-<?= $type; ?>">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?= $message ?>
          </div>
        <?php endforeach;?>
       <?php unset($_SESSION['flash']); ?>
      <?php endif;?>


