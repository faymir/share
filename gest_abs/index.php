<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Home Page</title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="home">
		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top headroom" >
			<div class="container">
				<div class="navbar-header">
					<!-- Button for smallest screens -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
					<a class="navbar-brand" href="index.php"><img src="assets/images/ensa.png" alt="ENSATE gestion d'absence"></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav pull-right">
						<li class="active"><a href="php/etudiant.php">Espace Etudiant</a></li>
						<li><a href="php/professeur.php">Espace Professeur</a></li>
						<li><a href="php/admin.php">Espace Administrateur</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
		<!-- /.navbar -->

		<!-- Header -->
		<header id="head">
			<div class="container">
				<div class="row">
					<h1 class="lead">Ecole Nationale Des Sciences Appliquées</h1>
					<p class="tagline">Crée en Septembre 2008, L’ École Nationale des Sciences Appliquées de Tétouan membre du réseau des Écoles Nationales des Sciences Appliquées, est un établissement public à caractère scientifique culturel et professionnel, instauré pour être une école d’ingénieurs de haut niveau.</p><br><br><br>
					<p><a class="btn btn-default btn-lg" role="button" href="php/register.php">S'inscrire</a>
				</div>
			</div>
		</header>
		<!-- /Header -->

		<!-- Intro -->
		<div class="container text-center">
			<br> <br>
			<h2 class="thin"><strong>ENSATE </strong>:Ecole National Des Science Appliquées</h2>
			<p class="text-muted">
				L’école a pour vocation principale de former des ingénieurs d’état rapidement opérationnel, particulièrement adaptable aux évolutions de la technologie et aux mutations de la société. Elle offre à ses étudiants une insertion professionnelle, à travers une pédagogie de l’autonomie et une adaptation technologique transdisciplinaire orientée vers l’innovation.</p>

			</p>
		</div>

		<footer id="footer" class="top-space">

			<div class="footer1">
				<div class="container">
					<div class="row">
						<div class="col-md-3 widget" >
							<h3 class="widget-title">Membres du Groupe</h3>
							<div class="widget-body" >
								<p>CHAOUI El Mehdi
									<br>CHERKAOUI RHAZOUANI Abdelkebir
									<br>GALLOUCH Hamza
									<br>TRAORE Fayçal Samir Zégué
								</p>
							</div><br><br>
						</div>
						<div class="col-md-3 widget">
							<h3 class="widget-title"></h3>
							<div class="widget-body">
							</div>
						</div>

						<div class="col-md-6 widget">
							<h3 class="widget-title">Supervisé Par :</h3>
							<div class="widget-body">
								<p>Professeur El YOUNOUSSI Yacine</p>
							</div>
						</div>

					</div> <!-- /row of widgets -->
				</div>
			</div>

	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>