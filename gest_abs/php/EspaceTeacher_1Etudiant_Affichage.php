<?php
require_once "../inc/db.php";
require_once "../inc/functions.php";
?><?php session_start();
if(!isset($_SESSION['auth']) || $_SESSION["type"]!="professeur"){
	header("Location:login.php");
	exit();
}
?><?php
require_once "../inc/header.php";
?><?php
	$cne=$_POST['etud'];
	$cnenull=0;
	if(empty($cne)){	$cnenull=1;	}
	
	if(!$cnenull)
	{
		$rep=$pdo->query("SELECT * FROM `etudiant` where cne=$cne ");
		$exist=0;
		$enregist1 = $rep->fetch();

		if($enregist1->cne==$cne){ 	 $exist=1; 	}
		
		if(!$exist)
		{
			$_SESSION['flash']['warning']='Le CNE ne correspond a aucun étudiant';
			header("Location:EspaceTeacher_1Etudiant.php");
			exit();
		}
			
	//}while(!$exist);

	//{
	if($exist)
	{
	
	echo'<div class="well">
  		<h5> Les données de l\'étudiant : <h5/>
		</div>';
		echo' <table class="table table-striped table-hover ">
			<h4>
			  <thead>
			    <tr>
			      <th>Nom :</th>
			      <th>'.$enregist1->nom.'</th>
			      <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
			    </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Prenom :</th>
			      <th>'.$enregist1->prenom.'</th>
			      <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
			    </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>CNE :</th>
			      <th>'.$enregist1->cne.'</th>
			      <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
			    </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Lieu de naissance :</th>
			      <th>'.$enregist1->lieu.'</th>
			      <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
			    </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Date de naissance :</th>
			      <th>'.$enregist1->date.'</th>
			      <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
			    </tr>
			  </thead>
			  <h4/>
			</table> 
		';
		echo '<br><br>';
		echo'<div class="well">
  		<h4>La liste des absences :<h4/>
		</div>';

		//importation des tables :
		$reponse2=$pdo->query("SELECT * FROM `absence` where  cne=$cne ");
		$reponse3=$pdo->query("SELECT * FROM `absence` where  cne=$cne ");

		//verification si on a aucune absence
		if( $enregist3=$reponse3->fetch()  ) { $nbr_abs=1; }
		else {	$nbr_abs=0;}
		
		//Affichage des absence :
		if($nbr_abs)
		{


			echo '

				<table class="table table-striped table-hover">
				  <thead>
				    <tr class="active">
				      	<th> Module </th>
						<th> L\'élément de module </th>
						<th> Type d\'enseignement </th>
						<th> Professeur </th>
						<th> Date </th>
						<th> Créneau </th>
						<th> Justifiée ? </th>
						<th> Justification </th>
				    </tr>
				  </thead>
				  
			';	
				$nbr=0;
				while( $enregist2=$reponse2->fetch()  )
				{	
				$nbr++;
					$reponse4=$pdo->prepare("SELECT * FROM `professeur` WHERE som= ?");
					$reponse4->execute([$enregist2->som]);
					$enregist4=$reponse4->fetch();

					
					//variable de la justification de l'absence

					
					if($enregist2->type)
						{
							$j='Oui';

						}
						else
						{
							$j='Non';
							$nbr_abs++; //Non justifiée
						}
					echo
					'<tbody>
						<tr>		
							<td> ' .$enregist2->module. '</td>
							<td> ' .$enregist2->element_module. '</td>
							<td> ' .$enregist2->courstdtp. '</td>
							<td> ' .$enregist4->nom.' '.$enregist4->prenom. '</td>
							<td> ' .$enregist2->date. '</td>
							<td> ' .$enregist2->creneau. '</td>
							<td> ' .$j. '</td>
							<td> ' .$enregist2->justification. '</td>
							
						</tr>
					</tbody>
					
					';
				}
			$n=$nbr_abs-1;
			echo '</table>';
			echo
				'
				  <table class="table table-striped table-hover">
					<tbody>
						<tr>
							<th><h4> Le nombre des absences  :</h4>
							<h4> Les absences totales : '.$nbr.' <br> Les absences non justifiées : '.$n.' </h4></th>
						</tr>
					</tbody>
				  </table>
					';
			echo '<button class="btn btn-default btn-primary btn-lg btn-block" onclick="myFunction()">La liste des absences</button>

					<script>
					function myFunction() {
					    window.print();
					}
					</script>
				<br><br>';
		
		}


		
		else
		{
			
			$_SESSION['flash']['success']='<h4>L\'étudiant n\'a aucune absence</h4>';
			echo'
			<div class="alert alert-dismissible alert-success">
			  <h4>L\'étudiant n\'a aucune absence</h4>
			</div>
			';
		}

		if ($nbr_abs>5)
		{



			$reponse3=$pdo->query("SELECT * FROM `etudiant` where  cne=$cne  ");
			$enregist3 = $reponse3->fetch();

			
			$_SESSION['flash']['danger']='<h4>Warning !</h4> L\'étudiant '.$enregist3->nom. ' ' .$enregist3->prenom.' a deppassé 4 absences non justifiées, veuillez l\'envoyer un message ';
			echo'
			
			<div class="alert alert-dismissible alert-danger">
			  <h4>Warning !</h4>
			  <p style="font-size:20px;">L\'étudiant '.$enregist3->nom. ' ' .$enregist3->prenom.' a deppassé 4 absences non justifiées, veuillez l\'envoyer un message </p>
			</div>
			';
		}
	
	  }
	}
	else 
	{
		$_SESSION['flash']['warning']='Le CNE ne correspond a aucun étudiant';
		header("Location:EspaceTeacher_1Etudiant.php");
		exit();
	}
	
	$conn = null;





?>


<?php
require_once "../inc/footer.php";
?>