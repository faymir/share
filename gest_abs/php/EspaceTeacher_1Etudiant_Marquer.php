<?php
require_once "../inc/db.php";
require_once "../inc/functions.php";
?>
<?php session_start();
if(!isset($_SESSION['auth']) || $_SESSION["type"]!="professeur"){
	header("Location:login.php");
	exit();
}
?>
<?php if(!empty($_POST)) {

    //debug($_POST);

    $req = $pdo->prepare("INSERT INTO absence SET code = DEFAULT ,courstdtp = ?, cne = ?, module = ?, element_module = ?,date = ?, creneau = ?,type = 0, som = ?,justification = DEFAULT");

    $i = 0;
    $module = '-';
    $element_module = '-';
    if (isset($_POST['module']))
        $module = $_POST['module'];
    if (isset($_POST['element_module']))
        $element_module = $_POST['element_module'];
    for ($i = 0; $i < $_POST['i']; $i++) {
        if ($_POST['status' . $i] == 'Absent') {
            $req->execute([$_POST['type'], $_POST['cne' . $i], $module, $element_module, $_POST['date'], $_POST['creneau'], $_SESSION['auth']->som]);
            $req2 = $pdo->prepare("UPDATE etudiant SET nbrAbs = nbrAbs + 1 WHERE cne = ?");
            $req2->execute([$_POST['cne' . $i]]);
        }
    }

    $_SESSION['flash']['success'] = "Liste d'absence du " . $_POST['date'] . " enregistrée avec succès";
}
?>
<?php
require_once "../inc/header.php";
?>


<div class="well">
	<h4>Marquage des absences : </h4>
</div>


<form action="" method="POST" ><br>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">


<!-- MODULE -->
	
	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Module <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
				<?php
				$reponse1=$pdo->query("SELECT * FROM `module` ");
				while($enregist1 = $reponse1->fetch()) {
					echo '<input type="radio" name="module" value="'. $enregist1->intitule_module.'"">'.$enregist1->intitule_module.' <br>';
				}
                ?>
	          </ul>
	        </li>
          </ul>


<!-- ELEMENT DE MODULE -->

	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Elément de module <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
                <?php
                    $reponse2=$pdo->query("SELECT * FROM `element_module` ");
                    while($enregist2 = $reponse2->fetch() ) {
                        echo '<div class="radio"><label><input type="radio" name="element_module" value="'. $enregist2->intitule_module.'"> '.$enregist2->intitule_module.'</label></div>';
                    }
                ?>

	          </ul>
	        </li>
	      </ul>

<!-- TYPE D'ENSEIGNEMENT -->

	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Type d'enseignement <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">

                  <div class="radio"><label><input type="radio" name="type" class="radio-inline" value="Cours" > Cours </label></div>
                  <div class="radio"><label><input type="radio" name="type" class="radio-inline" value="TD">TD </label></div>
                  <div class="radio"><label><input type="radio" name="type" class="radio-inline" value="TP">TP </label></div>
	          </ul>
	        </li>
	      </ul>


<!-- DATE -->

	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Date <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <input type="date" name="date" class="form-control" placeholder="aaaa-mm-jj" required>
                </ul>
	        </li>
	      </ul>


<!-- Creneau -->

	      <ul class="nav navbar-nav">
	        <li class="dropdown">
	          <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Créneau <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">

                  <div class="radio"><label><input type="radio" name="creneau" value="08-10"> 08-10</label></div>
                  <div class="radio"><label><input type="radio" name="creneau" value="10-12"> 10-12</label></div>
                  <div class="radio"><label><input type="radio" name="creneau" value="13-15"> 13-15</label></div>
                  <div class="radio"><label><input type="radio" name="creneau" value="15-17"> 15-17</label></div>
                  <div class="radio"><label><input type="radio" name="creneau" value="17-19"> 17-19</label></div>

	          </ul>
	        </li>
	      </ul>

	    </div>
	  </div>
	</nav>


	<div class="well">
	<h4>La liste des étudiants </h4>
    </div>

		<table class="table table-striped table-hover">
          <thead>
            <tr class="active">
                <th> Nom</th>
                <th> Prenom </th>
                <th> Présence </th>


            </tr>
          </thead>
            <tbody>
                <?php $reponse2=$pdo->query("SELECT * FROM `etudiant` ");?>
                <?php $i=0; while( $enregist2=$reponse2->fetch() ):?>
                    <tr>
                        <td><?=$enregist2->nom?></td>
                        <td><?=$enregist2->prenom?></td>
                        <td>
                            <input class="hidden" type="checkbox" name="status<?=$i?>" value="Present" checked>
                            <div class="checkbox"><label><input type="checkbox" name="status<?=$i?>" value="Absent" class="checkbox-inline -check-square"> Absent</label></div>
                              <input type="hidden" name="cne<?=$i?>" value="<?=$enregist2->cne?>">
                        </td>
                    </tr>
            </tbody>
            <?php $i++; endwhile;?>
            <input type="hidden" name="i" value="<?=$i?>">
		</table>
        <input type="submit" value="Enregistrer" class="btn btn-default " onclick="verif(); return false">
    </form>
<br><br><br><br>

<script>
    function verifCreneau() {
        var list = document.getElementsByName('creneau');
        for(var i=0;i<list.length;i++){
            if(list[i].checked)
                return true;
        }
        return false;
    }
    function verifEnseignement() {
        var list = document.getElementsByName('type');
        for(var i=0;i<list.length;i++){
            if(list[i].checked)
                return true;
        }
        return false;
    }
    function verifElement() {
        var list = document.getElementsByName('element_module');
        for(var i=0;i<list.length;i++){
            if(list[i].checked)
                return true;
        }
        return false;
    }

    function verif()
    {
        if(verifCreneau() && verifElement() && verifEnseignement())
            submit();
        else
            alert("cocher les champs requis");
    }
</script>

<?php
require_once "../inc/footer.php";
?>

