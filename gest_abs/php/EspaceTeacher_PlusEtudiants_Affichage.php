<?php

require_once "../inc/db.php";

session_start();
if(!isset($_SESSION['auth']) || ($_SESSION["type"]!="professeur" && $_SESSION['type']!='admin')){
	header("Location:login.php");
	exit();
}
?>
<?php
require_once "../inc/header.php";
?>
<?php
	
	echo'<div class="well">
  		<h4>Voici la liste d\'absence de tous les étudiants : <h4/>
		</div>';

		//importation des tables :
		$reponse2=$pdo->query("SELECT * FROM `absence` where  1 ");
		$reponse3=$pdo->query("SELECT * FROM `absence` where  1 ");

		//verification si on a aucune absence
		if( $enregist3=$reponse3->fetch()  ) { $nbr_abs=1; }
		else {	$nbr_abs=0;}
		
		//Affichage des absence :
		if($nbr_abs)
		{


			echo '

				<table class="table table-striped table-hover">
				  <thead>
				    <tr class="active">
				    	<th> CNE </th>
				    	<th> Nom </th>
				    	<th> Prenom </th>
				      	<th> Module </th>
						<th> L\'élément de module </th>
						<th> Type d\'enseignement </th>
						<th> Professeur </th>
						<th> Date </th>
						<th> Créneau </th>
						<th> Justifiée</th>
						<th> Justification </th>
						<th> Modifier </th>
				    </tr>
				  </thead>
				  
			';	
				$i=0;
            echo '<form action ="justifyAbs.php" method="POST">';
				while( $enregist2=$reponse2->fetch()  )
				{	
					//Pour le nom du professeur
					$reponse4=$pdo->prepare("SELECT * FROM `professeur` WHERE som= ?");
					$reponse4->execute([$enregist2->som]);
					$enregist4=$reponse4->fetch();

					//Pour le nom de l'etudiant
					$reponse5=$pdo->prepare("SELECT * FROM `etudiant` WHERE cne= ?");
					$reponse5->execute([$enregist2->cne]);
					$enregist5=$reponse5->fetch();

					
					
					//variable de la justification de l'absence

					
					if($enregist2->type)
						{
							$j='Oui';

						}
						else
						{
							$j='Non';
							$nbr_abs++; //Non justifiée
						}
					echo
					'<tbody>
						<tr>		
							<td> ' .$enregist2->cne. '</td>
							<td> ' .$enregist5->nom. '</td>
							<td> ' .$enregist5->prenom. '</td>
							<td> ' .$enregist2->module. '</td>
							<td> ' .$enregist2->element_module. '</td>
							<td> ' .$enregist2->courstdtp. '</td>
							<td> ' .$enregist4->nom.' '.$enregist4->prenom. '</td>
							<td> ' .$enregist2->date. '</td>
							<td> ' .$enregist2->creneau. '</td>
							<td> ' .$j. '</td>
							<td> ' .$enregist2->justification. '</td>
							<td><input type="checkbox" name="check'.$i.'" value="'.$enregist2->code.'" class="checkbox-inline -check-circle"></td>
						    
                            <input type="hidden" name="nom'.$i.'" value="'.$enregist5->nom.'">
                            <input type="hidden" name="prenom'.$i++.'" value="'.$enregist5->prenom.'">
						</tr>
					</tbody>
					
					';
				}
			
			echo '</table>';
            echo '
                <input type="hidden" name="i" value="'.$i.'">
                <input type="submit" class="btn btn-default btn-primary btn-lg btn-block" onclick="verif(); return false" value="Modifier">
                </form>
            ';
			echo '<button class="btn btn-default btn-primary btn-lg btn-block" onclick="myFunction()">La liste des absences</button>

					<script>
					function myFunction() {
					    window.print();
					}
					function verif() {
					    var n = '.$i.';
					    var ok =false;
					    for(var i =0 ;i<n;i++){
					    var a=document.getElementsByName("check"+i)[0];
					    if(a.checked){
					    ok = true;
					    break;
					    }
					  }
					  if(ok)
					    submit();
					  else
					    alert("Veuillez d\'abord sélectionner un champ");
					}
					</script>
				<br><br>';
		}
		
	$conn = null;
?>

<?php
require_once "../inc/footer.php";
?>