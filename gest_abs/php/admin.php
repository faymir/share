<?php require_once "../inc/functions.php"; ?>
<?php session_start();

	if(!isset($_SESSION['auth']) || $_SESSION['type']!='admin'){
		$_SESSION['flash']['danger']="Veuillez d'abord vous Connecter en tant qu'admin pour accéder à cette page";
		header('Location: ../php/login.php');
		exit();
}

?>
<?php require '../inc/header.php' ?>
<?php require_once "../inc/footer.php" ?>