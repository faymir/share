<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 23-May-17
 * Time: 14:56
 */
?><?php require '../inc/functions.php';?><?php
session_start();
if(!isset($_SESSION['auth']) || $_SESSION['type']!='admin'){
    $_SESSION['flash']['danger']="Veuillez d'abord vous Connecter en tant qu'admin pour accéder à cette page";
    header('Location: ../php/login.php');
    exit();
}
?>
<?php
if(!empty($_POST) && !empty($_POST['username']) && !empty($_POST['type']) && !empty($_POST['message']))
{
    require_once '../inc/db.php';
    $reqAdmin = $pdo->prepare('INSERT INTO messages VALUES (DEFAULT ,?,?,?,?)');
    $reqAdmin->execute([$_POST['username'],'admin',$_POST['message'],$_POST['type']]);	$_SESSION['flash']['success'] = 'Message envoyé';
}
else if(!empty($_POST)){
    $_SESSION['flash']['info'] = 'Veuillez bien remplir les champs';
}

?>
<?php require '../inc/header.php'; ?>

<div class="container">

    <form action="" method="post" class=" jumbotron">
        <h2 class="form-signin-heading text-center" >Message</h2>
        <label for="">Pseudo ou email destinataire</label>
        <input  type="text" name="username" class="form-control" placeholder="Pseudo ou email"
            <?php
            if (!empty($errors) && isset($_POST["username"]) && !isset($errors['username'])){
                echo "value=".$_POST['username'];
            }
            ?>>
        <label for="" class="">Type</label>
        <input type="text" name="type" class="form-control" placeholder="danger,warning,active,info" />
        <label for="textarea" class="">Message</label>
        <textarea name="message" id='textarea' class="form-control" rows="8" cols="60"></textarea>
        <button type="submit" class="btn btn-default btn-block">Envoyer</button>
    </form>

</div> <!-- /container -->



<?php require '../inc/footer.php'; ?>
