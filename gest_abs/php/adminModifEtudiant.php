<?php require '../inc/functions.php';?>
<?php require '../inc/header.php'; ?>
<?php if(!isset($_SESSION['auth']) || $_SESSION['type']!='admin'){
    $_SESSION['flash']['danger']="Veuillez d'abord vous Connecter en tant qu'admin pour accéder à cette page";
    header('Location: ../php/login.php');
    exit();
}
require_once "../inc/db.php";
    if(!empty($_POST))
    {
            if($_POST['Button']=='Modifier' && isset($_POST['modif'])) {
                $tmp='Location: modifEtudiant.php?modif='.$_POST['modif'];
                header($tmp);
                exit();
            }
            if($_POST['Button']=='Modifier' && !isset($_POST['modif']))
            {
                $_SESSION['flash']['warning']="Veuillez d'abord selectionner un champ a modifier";
            }

            if($_SESSION['nbrEleve']>0 && $_POST['Button']=='Valider')
            {
                for ($z=0;$z<$_SESSION['nbrEleve'];$z++){
                    if(isset($_POST['supp'.$z]))
                    {
                        $tmp = "DELETE FROM etudiant WHERE id=".$_POST['supp'.$z];
                        if($pdo->query($tmp) === TRUE )
                            $_SESSION['flash']['success']="Suppression effectuée";
                        else
                            $_SESSION['flash']['danger']="Erreur lors de la suppression";
                    }
                    if(isset($_POST['reset'.$z]))
                    {
                        $tmp = "UPDATE etudiant SET nbrAbs =0 WHERE id=".$_POST['reset'.$z];
                        if($pdo->query($tmp) === TRUE ) {
                            $_SESSION['flash']['success']="Modification effectuée";
                        }
                        else{
                            $_SESSION['flash']['danger']="Echec de la modification";
                        }
                    }

                }
            }
    }

    $req = $pdo->prepare('SELECT * FROM etudiant');
    $req->execute();
?>
    <script language="JavaScript">
        function toggle(source,nom) {
            var checkboxes = document.getElementsByClassName(nom);
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = source.checked;
            }
            function traiter() {
                var x = document.getElementsByName('modif');
                x = x[0].checked;
                document.location.href='modifEtudiant.php?modif='+x;
            }
        }
    </script>

<form action="" method="post" class="">
    <table class="table table-striped table-hover jumbotron">
        <thead>
        <tr>
            <th>Supprimer</th>
            <th>Remettre à zéro </th>
            <th>Modifier</th>
            <th>Username</th>
            <th>CNE</th>
            <th>CIN</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date naissance</th>
            <th>Lieu</th>
            <th>Adresse</th>
            <th>Téléphone</th>
            <th>Email</th>
            <th>Absences</th>
        </tr>
        </thead>
        <tbody>
        <?php $valeur='active'?>
        <?php $i=0;  while($result = $req->fetch()): ?>
                <tr class="<?=$valeur?>>">
                    <td class="text-center"><input type="checkbox" name="<?='supp'.$i?>" value="<?=$result->id?>" class="maCheck foo checkbox-inline -check-circle"></td>
                    <td class="text-center"><input type="checkbox" name="<?='reset'.$i?>" value="<?=$result->id?>" class="maCheck foo2 checkbox-inline -check-circle"></td>
                    <td class="text-center"><input type="radio" name="modif" value="<?=$result->id?>" class="checkbox-inline -check-circle"></td>
                    <td> <?=$result->username?> </td>
                    <td> <?=$result->cne?> </td>
                    <td> <?=$result->cin?> </td>
                    <td> <?=$result->nom?> </td>
                    <td> <?=$result->prenom?> </td>
                    <td> <?=$result->date?> </td>
                    <td> <?=$result->lieu?> </td>
                    <td> <?=$result->adresse?> </td>
                    <td> <?=$result->telephone?> </td>
                    <td> <?=$result->email?> </td>
                    <td class="text-right"> <?=$result->nbrAbs?> </td>
                </tr>
            <?php $i++; if($valeur=='active'){$valeur='';}else{$valeur='active';}?>
        <?php endwhile;$_SESSION['nbrEleve']=$i;?>
        </tbody>
    </table>
    <div class="form-group">
        <label><input type="checkbox" id="supprimer" onClick="toggle(this,'foo')" class="maRadio checkbox-inline -check-circle"> Tout Supprimer</label>
        <label><input type="checkbox" id="mettreZero"  onClick="toggle(this,'foo2')" class="maRadio checkbox-inline -check-circle"> Remettre à zéro</label>
    </div>
    <div class="form-group">
        <input name="Button" type="submit" class="btn btn-default" value="Valider" >
        <input name="Button" type="submit" class="btn btn-default" value="Modifier">
    </div>

</form>

<?php require '../inc/footer.php';/* onclick="document.location.href='modifEtudiant.php?mavariable=test'*/ ?>