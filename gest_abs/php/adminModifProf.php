<?php
/**
 * Created by PhpStorm.
 * User: Toshia
 * Date: 25-May-17
 * Time: 08:16
 */
?><?php require '../inc/functions.php';?><?php require '../inc/header.php'; ?><?php if(!isset($_SESSION['auth'])  || $_SESSION['type']!='admin'){
    $_SESSION['flash']['danger']="Veuillez d'abord vous Connecter pour accéder à cette page";
    header('Location: ../php/login.php');
    exit();
}
require_once "../inc/db.php";
$req = $pdo->prepare('SELECT * FROM professeur');
$req->execute();
?>
    <div class="container">

        <script language="JavaScript">
            function toggle(source,nom) {
                var checkboxes = document.getElementsByClassName(nom);
                for(var i=0, n=checkboxes.length;i<n;i++) {
                    checkboxes[i].checked = source.checked;
                }
            }
        </script>


        <table class="table table-striped table-hover jumbotron">
            <thead>
            <tr>
                <th>Modifier</th>
                <th>Username</th>
                <th>SOM</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Téléphone</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php $valeur='active'?>
            <?php  while($result = $req->fetch()): ?>
                <tr class="<?=$valeur?>>">
                    <td class="text-center"><input type="radio" name="modif" value="<?=$result->id?>" class="foo checkbox-inline -check-circle"></td>
                    <td> <?=$result->username?> </td>
                    <td> <?=$result->som?> </td>
                    <td> <?=$result->nom?> </td>
                    <td> <?=$result->prenom?> </td>
                    <td> <?=$result->telephone?> </td>
                    <td> <?=$result->email?> </td>
                </tr>
                <?php if($valeur=='active'){$valeur='';}else{$valeur='active';}?>
            <?php endwhile;?>
            </tbody>
        </table>
        <button type="submit" class="btn btn-default navbar-center">Valider</button>

    </div> <!-- /container -->

<?php require '../inc/footer.php'; ?>