<?php
require_once "../inc/db.php";
require_once "../inc/functions.php";
session_start();
if(isset($_SESSION['auth']) && $_SESSION['type']=='etudiant')){
    $req = $pdo->prepare("SELECT COUNT(*) AS compteur FROM absence WHERE cne = ? AND type=0");
    $req->execute([$_SESSION['auth']->cne]);
    $res=$req->fetch();
    if($res->compteur >=4){
        $_SESSION['flash']['danger']='Vous avez depassé 4 absences non jusifiées, veuillez contacter la direction ';
        $req2 = $pdo->prepare('SELECT * FROM messages WHERE message = ?');
        $req2->execute(["Vous avez depassé 4 absences non jusifiées, veuillez contacter la direction"]);
        $user = $req2->fetch();
        if(!$user) {
            $reqAdmin = $pdo->prepare('INSERT INTO messages VALUES (DEFAULT ,?,?,?,?)');
            $reqAdmin->execute([$_SESSION['auth']->username, 'sytem', "Vous avez depassé 4 absences non jusifiées, veuillez contacter la direction", 'danger']);
        }
    }
}
?>
<?php
require_once "../inc/header.php";
?>
<?php

	
	$cne=$_SESSION['auth']->cne;


	echo'<div class="well">
  		<h4>Welcome !<h4/><h5> Voici vos données personnelles : <h5/>
		</div>';
		echo' <table class="table table-striped table-hover ">
			<h4>
			  <thead>
			    <tr>
			      <th>Nom :</th>
			      <th>'.$_SESSION['auth']->nom.'</th>
			      </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Prenom :</th>
			      <th>'.$_SESSION['auth']->prenom.'</th>
			      </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>CNE :</th>
			      <th>'.$_SESSION['auth']->cne.'</th>
			      </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Lieu de naissance :</th>
			      <th>'.$_SESSION['auth']->lieu.'</th>
			      </tr>
			  </thead>
			  <thead>
			    <tr>
			      <th>Date de naissance :</th>
			      <th>'.$_SESSION['auth']->date.'</th>
			      </tr>
			  </thead>
			  <h4/>
			</table> 
		';
		echo '<br><br>';
		echo'<div class="well">
  		<h4>Vos absences :<h4/>
		</div>';

		//importation des tables :
		$reponse2=$pdo->query("SELECT * FROM `absence` where  cne=$cne ");
		$reponse3=$pdo->query("SELECT * FROM `absence` where  cne=$cne ");
		

		//verification si on a aucune absence
		if( $enregist3=$reponse3->fetch()  ) { $nbr_abs=1; }
		else {	$nbr_abs=0;}
		
		//Affichage des absence :
		if($nbr_abs)
		{


			echo '

				<table class="table table-striped table-hover">
				  <thead>
				    <tr class="active">
				      	<th> Module </th>
						<th> L\'élément de module </th>
						<th> Type d\'enseignement </th>
						<th> Professeur </th>
						<th> Date </th>
						<th> Créneau </th>
						<th> Justifiée ? </th>
						<th> Justification </th>
				    </tr>
				  </thead>
				  
			';	
				
				while( $enregist2=$reponse2->fetch()  )
				{
					$reponse4=$pdo->prepare("SELECT * FROM `professeur` WHERE som= ?");
					$reponse4->execute([$enregist2->som]);
					$enregist4=$reponse4->fetch();

					
					//variable de la justification de l'absence

					
					if($enregist2->type)
						{
							$j='Oui';

						}
						else
						{
							$j='Non';
							$nbr_abs++; //Non justifiée
						}
					echo
					'<tbody>
						<tr>
							<td> ' .$enregist2->module. '</td>
							<td> ' .$enregist2->element_module. '</td>
							<td> ' .$enregist2->courstdtp. '</td>
							<td> ' .$enregist4->nom.' '.$enregist4->prenom.'</td>
							<td> ' .$enregist2->date. '</td>
							<td> ' .$enregist2->creneau. '</td>
							<td> ' .$j. '</td>
							<td> ' .$enregist2->justification. '</td>
						</tr>
					</tbody>
					
					';
				}
			
			echo '</table>';
		}
		
		else
		{
			/*$_SESSION['flash']['success']='<h4>Congratulation ! Vous n\'avez aucune absence</h4>';*/
			echo'
			<div class="alert alert-dismissible alert-success">
			  <h4>Felicitation ! Vous n\'avez aucune absence</h4>
			</div>
			';
		}

echo '<a href="" class="btn btn-default btn-primary btn-lg btn-block">Télécharger la liste de vos absences</a><br>';
	$conn = null;
?>

<?php
require_once "../inc/footer.php";
?>