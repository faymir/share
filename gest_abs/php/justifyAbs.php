<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 29-May-17
 * Time: 20:51
 */
?><?php	session_start();

require_once "../inc/db.php";
require_once "../inc/functions.php";



if(!isset($_SESSION['auth']) || ($_SESSION["type"]!="professeur" && $_SESSION['type']!='admin')){
    header("Location:login.php");
    exit();
}

if(isset($_POST['i2'])){
    //debug($_POST);
    $i=0;
    $req = $pdo->prepare("UPDATE absence SET type = 1,justification = ? WHERE code = ?");
    for ($i=0;$i<$_POST['i2'];$i++)
    {
        if(isset($_POST['justificatif'.$i])){
            $req->execute([$_POST['justificatif'.$i],$_POST['code'.$i]]);
        }
    }

    header("Location:EspaceTeacher_PlusEtudiants_Affichage.php");
    exit();
}
?>
<?php
require_once "../inc/header.php";
?>
<form method="post" action="" xmlns="http://www.w3.org/1999/html">
    <table class="table table-striped table-hover">
        <thead>
        <tr class="active">
            <th> Nom </th>
            <th> Prenom </th>
            <th> Justificatif </th>
        </tr>
        </thead>
        <tbody>

<?php for ($z=0;$z<$_POST['i'];$z++):?>
        <?php if(isset($_POST['check'.$z])): ?>
        <tr>
            <td><?=$_POST['nom'.$z]?></td>
            <td><?=$_POST['prenom'.$z]?></td>
            <td><input type="text" name="justificatif<?=$z?>"></td>
            <input type="hidden" name="code<?=$z?>" value="<?=$_POST['check'.$z]?>">
        </tr>
        <?php endif;?>
<?php endfor;?>
        </tbody>
     </table>

    <input type="hidden" name="i2" value="<?=$_POST['i']?>">
    <div class="form-group">
        <button type="submit" class="btn btn-default">Valider</button>
        <button type="button" class="btn btn-default"><a style="color: inherit" href="../php/EspaceTeacher_PlusEtudiants_Affichage.php">Annuler</a></button>
    </div>
</form>
