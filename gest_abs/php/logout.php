<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 23-May-17
 * Time: 22:36
 */
?>
<?php 	session_start();	if(isset($_SESSION['auth']))	{
		unset($_SESSION['auth']);
		unset($_SESSION['type']);
		$_SESSION['flash']['success'] = 'Vous êtes maintenant déconnecté';
	}
?>
<?php header('Location:login.php');exit();?>