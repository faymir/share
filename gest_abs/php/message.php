<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 23-May-17
 * Time: 14:56
 */
?>
<?php require '../inc/functions.php';?>
<?php session_start();
    if(!isset($_SESSION['auth'])){
    $_SESSION['flash']['danger']="Veuillez d'abord vous Connecter pour accéder à cette page";
    header('Location: ../php/login.php');
    exit();
}
require_once "../inc/db.php";
if(!empty($_POST))
{

    if($_SESSION['nbrMessage']>0)
    {
        $z=0;
        for ($z=0;$z<$_SESSION['nbrMessage'];$z++){
            if(isset($_POST['case'.$z]))
            {
                $tmp = "DELETE FROM messages WHERE id=?";
                $requete = $pdo->prepare($tmp);
                if($requete->execute([$_POST['case'.$z]]) === TRUE )
                    $_SESSION['flash']['success']="Suppression effectuée";
                else
                    $_SESSION['flash']['danger']="Erreur lors de la suppression";
            }
        }
        $_SESSION['nbrMessage']-=$z;
    }
}
    $req = $pdo->prepare('SELECT * FROM messages WHERE username = ?');
    $req->execute([$_SESSION['auth']->username]);

    $result = $req->fetchObject();
    if(is_object($result)){
        $type=$result->type;
        $sender=$result->sender; $message=$result->message; $id=$result->id;
    }
?>
<?php require '../inc/header.php'; ?>
<form action="" method="post" class="">

    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th>Supprimer</th>
            <th>De</th>
            <th>Message</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        <?php if($_SESSION['nbrMessage']!=0): ?>
            <?php $req = $pdo->prepare('SELECT * FROM messages WHERE username = ? OR username = ?');
                $req->execute([$_SESSION['auth']->username,$_SESSION['auth']->email]);
                   for ($i=0; $i < $_SESSION['nbrMessage']; $i++):?>
                 <?php
                $result = $req->fetch(); $type=$result->type; $sender=$result->sender; $message=$result->message; $id=$result->id;
                ?>
                <tr class="<?=$type?>">
                    <td><input type="checkbox" name="<?='case'.$i ?>" value="<?=$id?>" class="checkbox-inline -check-circle"></td>
                    <td> <?=$sender?> </td>
                    <td><pre><?php echo $message; ?> </pre></td>
                    <td> <?=$i+1?></td>
                </tr>
            <?php endfor;?>
            <?php
            ?>
        <?php endif;?>
        </tbody>
    </table>
    <button type="submit" class="btn btn-default navbar-center	">Valider</button>
</form>


<?php require '../inc/footer.php'; ?>



