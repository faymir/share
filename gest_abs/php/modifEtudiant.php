<?php
session_start();
require '../inc/functions.php';
require '../inc/db.php';
?>
<?php if(!isset($_SESSION['auth']) || $_SESSION['type']!='admin'){
    $_SESSION['flash']['danger']="Veuillez d'abord vous Connecter en tant qu'admin pour accéder à cette page";
    header('Location: ../php/login.php');
    exit();
}

    $result=NULL;
    $nom = "";
    $prenom = "";
    $username = "";
    $cne = "";
    $cin = "";
    $date = "";
    $lieu = "";
    $adresse = "";
    $phone = "";
    $email = "";
if(!empty($_POST)) {
    $errors = array();

    if (empty($_POST['nom']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ[:blank:]-]+)$/', $_POST['nom'])) {
        $errors['nom'] = "Votre nom n'est pas valide (alphanumerique)";
    }
    if (empty($_POST['prenom']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ[:blank:]-]+)$/', $_POST['prenom'])) {
        $errors['prenom'] = "Votre prénom n'est pas valide (alphanumerique)";
    }
    if (empty($_POST['username']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ-]+)$/', $_POST['username'])) {
        $errors['username'] = "Votre pseudo n'est pas valide (alphanumerique sans espace)";
    }
    else
    {
        $reqP = $pdo->prepare('SELECT id FROM professeur WHERE username = ?');
        $reqA = $pdo->prepare('SELECT id FROM admin WHERE username = ?');
        $reqP->execute([$_POST['username']]);
        $reqA->execute([$_POST['username']]);
        $userP = $reqP->fetch();
        $userA = $reqA->fetch();

        if($userA || $userP){
            $errors['username'] = 'Ce pseudonyme est déjà utilisé pour un autre compte';
        }
    }

    if (empty($_POST['cne']) || !filter_var( $_POST['cne'],FILTER_VALIDATE_INT) || strlen($_POST['cne'])!=10) {
        $errors['cne'] = "Votre CNE n'est pas valide (code 10 chiffres)";
    }
    else
    {
        $req2 = $pdo->prepare('SELECT cne FROM liste_cne WHERE cne = ?');
        $req2->execute([$_POST['cne']]);
        $user2 = $req2->fetch();

        if(!$user2){
            $errors['cne_non_autorise'] = "Ce CNE ne figurre pas dans la liste des etudiants de GI1 veuillez contacter l'administrateur";
        }
    }
    if (empty($_POST['cin']) || !preg_match('/^([a-zA-Z0-9]+)$/',$_POST['cin']) || strlen($_POST['cin'])!=8) {
        $errors['cin'] = "Votre CIN n'est pas valide (code 8 caractere alphanumeriques)";
    }

    if (empty($_POST['naissance']) || !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',$_POST['naissance'])) {
        $errors['naissance'] = "Votre date de naissance n'est pas valide";
    }

    if (empty($_POST['lieuNaissance'])) {
        $errors['lieuNaissance'] = "Veuillez entrer un lieu de naissance valide";
    }
    if (empty($_POST['adresse'])) {
        $errors['adresse'] = "Veuillez entrer une adresse valide";
    }

    if (empty($_POST['phone']) ||!preg_match('/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/', $_POST['phone'])){
        $errors['phone'] = "Veuillez entrer un numéro de téléphone valide";
    }

    if (empty($_POST['email']) || !filter_var( $_POST['email'],FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Veuillez entrer un mail valide (example@mail.com)";
    }

    if (empty($_POST['password']) || strlen($_POST['password'])<6 || empty($_POST['password_confirm']) || $_POST['password']!=$_POST['password_confirm']){
        $errors['password'] = "Veuillez entrer un mot de passe valide (taille supérieure a 6 et deux mots de passes identiques)";
    }


    if(empty($errors)) {
        $req = $pdo->prepare("UPDATE etudiant SET username = ?, password = ?, cne = ?, cin = ?, nom = ?, prenom = ?,date = ?, lieu = ?,adresse = ?, telephone = ?,email = ?");
        $password = password_hash($_POST['password'],PASSWORD_BCRYPT);
        $req->execute([$_POST['username'],$password,$_POST['cne'],$_POST['cin'],$_POST['nom'],$_POST['prenom'],$_POST['naissance'],$_POST['lieuNaissance'],$_POST['adresse'],$_POST['phone'],$_POST['email']]);
        session_start();
        $_SESSION['flash']['success']= 'Le compte a été modifié avec success';
        header('Location:adminModifEtudiant.php');
        exit();
    }
}
else if(!empty($_GET['modif'])) {
    $req = $pdo->prepare("SELECT * FROM etudiant WHERE id = ?");
    $req->execute([$_GET['modif']]);

    $result=$req->fetchObject();
    $nom = $result->nom;
    $prenom = $result->prenom;
    $username = $result->username;
    $cne = $result->cne;
    $cin = $result->cin;
    $date = $result->date;
    $lieu = $result->lieu;
    $adresse = $result->adresse;
    $phone = $result->telephone;
    $email = $result->email;

}
?>
<?php require  '../inc/header.php'; ?>


<?php if(!empty($errors)): ?>
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <ul>
            <?php foreach($errors as $i): ?>
                <li><?= $i; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<h2>S'inscrire</h2>
<form action="" method="post" class="form-horizontal jumbotron container">

    <div class="form-group">
        <label for="">Nom</label>
        <input type="text" name="nom" class="form-control" autocomplete="on" placeholder="nom"
            <?php
            if (!empty($errors) && isset($_POST["nom"]) && !isset($errors['nom'])){
                echo "value=".$_POST['nom'];
            }
            if($result!=NULL){
                echo "value=".$nom;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="">Prénom</label>
        <input type="text" name="prenom" class="form-control" autocomplete="on" placeholder="prénom"
            <?php
            if (!empty($errors) && isset($_POST["prenom"]) && !isset($errors['prenom'])){
                echo "value=".$_POST['prenom'];
            }

            if($result!=NULL){
                echo "value=".$prenom;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="">Pseudo</label>
        <input type="text" name="username" class="form-control" autocomplete="on" placeholder="Pseudo"
            <?php
            if (!empty($errors) && isset($_POST["username"]) && !isset($errors['username'])){
                echo "value=".$_POST['username'];
            }

            if($result!=NULL){
                echo "value=".$username;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="" id="">CNE</label>
        <input type="number" name="cne" class="form-control" autocomplete="on" placeholder="Code CNE"
            <?php
            if (!empty($errors) && isset($_POST["cne"]) && !isset($errors['cne'])){
                echo "value=".$_POST['cne'];
            }

            if($result!=NULL){
                echo "value=".$cne;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="" id="">CIN</label>
        <input type="text" name="cin" class="form-control" autocomplete="on" placeholder="Code CIN"
            <?php
            if (!empty($errors) && isset($_POST["cin"]) && !isset($errors['cin'])){
                echo "value=".$_POST['cin'];
            }

            if($result!=NULL){
                echo "value=".$cin;
            }
            ?>>
    </div>

    <div class="form-group">
        <label for="">Date de naissance</label>
        <input type="date" name="naissance" class="form-control" autocomplete="on" placeholder="aaaa-mm-jj"
            <?php
            if (!empty($errors) && isset($_POST["naissance"]) && !isset($errors['naissance'])){
                echo "value=".$_POST['naissance'];
            }

            if($result!=NULL){
                echo "value=".$date;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="">Lieu de naissance</label>
        <input type="text" name="lieuNaissance" class="form-control" autocomplete="on" placeholder="Rabat"
            <?php
            if (!empty($errors) && isset($_POST["lieuNaissance"]) && !isset($errors['lieuNaissance'])){
                echo "value=".$_POST['lieuNaissance'];
            }

            if($result!=NULL){
                echo "value=".$lieu;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="">Adresse</label>
        <input type="text" name="adresse" class="form-control" autocomplete="on" placeholder="Votre adresse"
            <?php
            if (!empty($errors) && isset($_POST["adresse"]) && !isset($errors['adresse'])){
                echo "value=".$_POST['adresse'];
            }

            if($result!=NULL){
                echo "value=".$adresse;
            }
            ?>>
    </div>

    <div class="form-group">
        <label for="">Téléphone</label>
        <input type="tel" name="phone" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" autocomplete="on" placeholder="0600000000"
            <?php
            if (!empty($errors) && isset($_POST["phone"]) && !isset($errors['phone'])){
                echo "value=".$_POST['phone'];
            }

            if($result!=NULL){
                echo "value=".$phone;
            }
            ?>>
    </div>
    <div class="form-group">
        <label for="">Email</label>
        <input type="email" name="email" class="form-control" autocomplete="on" placeholder="example@mail.com"
            <?php
            if (!empty($errors) && isset($_POST["email"]) && !isset($errors['email'])){
                echo "value=".$_POST['email'];
            }

            if($result!=NULL){
                echo "value=".$email;
            }
            ?>>
    </div>

    <div class="form-group">
        <label for="">Mot de passe</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Confirmer mot de passe</label>
        <input type="password" name="password_confirm" class="form-control" />
    </div>

    <button type="submit" class="btn btn-default">Modifier</button>
</form>



<?php require  '../inc/footer.php'; ?>
