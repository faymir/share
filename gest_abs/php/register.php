<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 23-May-17
 * Time: 10:02
 *
 * header("refresh:5;url=wherever.php" );
 */
?>
<?php require_once "../inc/functions.php" ?>
<?php
    session_start();
    if(isset($_SESSION['auth']) && isset($_SESSION['type']))
    {
        $_SESSION['flash']['danger']="Veuillez d'abord vous déconnecter";
        if($_SESSION['type']=='admin')
            header("Location:admin.php" );

        if($_SESSION['type']=='etudiant')
            header("Location:etudiant.php" );

        if($_SESSION['type']=='professeur')
            header("Location:professeur.php" );

        exit();
    }
?>
<?php
    $errors = array();
    if(!empty($_POST)) {

        if($_POST['typeCompte']=='student'){
            header('location:registerStudent.php');
        }
        elseif ($_POST['typeCompte']=='teacher'){
            header('location:registerTeacher.php');
        }

    }
    else{
        $errors['choixCompte'] = "Veuillez choisir un type de compte";
    }
?>
<?php require  '../inc/header.php'; ?>
<!--br><br><br-->
<h1>S'inscrire</h1>


<?php if(!empty($errors) && empty($_POST)): ?>
    <div class="alert alert-warning">
        <p>Veuillez choisir un des types de comptes</p>
    </div>
<?php endif; ?>

<form action="" method="post">

    <div class="form-group">
        <label class="control-label">Créer un compte de type </label>
        <div>
            <div class="radio">
                <label>
                    <input type="radio" name="typeCompte" id="ietudiant" value="student">
                    Etudiant
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="typeCompte" id="iprof" value="teacher">
                    Professeur
                </label>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-default">M'inscrire</button>
</form>



<?php require  '../inc/footer.php'; ?>
