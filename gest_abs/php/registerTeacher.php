<?php
/**
 * Created by PhpStorm.
 * User: Faymir
 * Date: 21-May-17
 * Time: 19:49
 */
?>
<?php
    session_start();
    require_once '../inc/functions.php';

    if(isset($_SESSION['auth']))
    {
        $_SESSION['flash']['info']="Veuillez d'abord vous déconnecter";
        if($_SESSION['type']=='admin')
            header("Location:admin.php" );

        if($_SESSION['type']=='etudiant')
            header("Location:etudiant.php" );

        if($_SESSION['type']=='professeur')
            header("Location:professeur.php" );

        exit();

    }
    if(!empty($_POST)) {


        $errors = array();
        require_once '../inc/db.php';

        if (empty($_POST['nom']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ[:blank:]-]+)$/', $_POST['nom'])) {
            $errors['nom'] = "Votre nom n'est pas valide (alphanumerique)";
        }
        if (empty($_POST['prenom']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ[:blank:]-]+)$/', $_POST['prenom'])) {
            $errors['prenom'] = "Votre prénom n'est pas valide (alphanumerique)";
        }
        if (empty($_POST['username']) || !preg_match('/^([a-zA-Z\'àâéèêôùûçÀÂÉÈÔÙÛÇ-]+)$/', $_POST['username'])) {
            $errors['username'] = "Votre pseudo n'est pas valide (alphanumerique sans espace)";
        }
        else
        {
            $reqE = $pdo->prepare('SELECT id FROM etudiant WHERE username = ?');
            $reqP = $pdo->prepare('SELECT id FROM professeur WHERE username = ?');
            $reqA = $pdo->prepare('SELECT id FROM admin WHERE username = ?');
            $reqE->execute([$_POST['username']]);
            $reqP->execute([$_POST['username']]);
            $reqA->execute([$_POST['username']]);
            $userE = $reqE->fetch();
            $userP = $reqP->fetch();
            $userA = $reqA->fetch();

            if($userA || $userE || $userP){
                $errors['username'] = 'Ce pseudonyme est déjà utilisé pour un autre compte';
            }
        }

        if (empty($_POST['som']) || !filter_var( $_POST['som'],FILTER_VALIDATE_INT) || strlen($_POST['som'])!=7) {
            $errors['som'] = "Votre SOM n'est pas valide (code 7 chiffres)";
        }
        else
        {
            $req = $pdo->prepare('SELECT id FROM professeur WHERE som = ?');
            $req->execute([$_POST['som']]);
            $user = $req->fetch();

            if($user){
                $errors['som'] = 'Ce SOM est déjà utilisé pour un autre compte';
            }

            $req2 = $pdo->prepare('SELECT som FROM liste_som WHERE som = ?');
            $req2->execute([$_POST['som']]);
            $user2 = $req2->fetch();

            if(!$user2){
                $errors['som_non_autorise'] = "Ce SOM ne figurre pas dans la liste des Professeurs de GI1 veuillez contacter l'administrateur";
            }
        }

        if (empty($_POST['phone']) ||!preg_match('/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/', $_POST['phone'])){
            $errors['phone'] = "Veuillez entrer un numéro de téléphone valide";
        }

        if (empty($_POST['email']) || !filter_var( $_POST['email'],FILTER_VALIDATE_EMAIL)){
            $errors['email'] = "Veuillez entrer un mail valide (example@mail.com)";
        }
        else
        {
            $req = $pdo->prepare('SELECT id FROM professeur WHERE email = ?');
            $req->execute([$_POST['email']]);
            $user = $req->fetch();

            if($user){
                $errors['email'] = 'Cet email est déjà utilisé pour un autre compte';
            }
        }

        if (empty($_POST['password']) || strlen($_POST['password'])<6 || empty($_POST['password_confirm']) || $_POST['password']!=$_POST['password_confirm']){
            $errors['password'] = "Veuillez entrer un mot de passe valide (taille supérieure a 6 et deux mots de passes identiques)";
        }


        if(empty($errors)) {
            $req = $pdo->prepare("INSERT INTO professeur SET username = ?, password = ?, som = ?, nom = ?, prenom = ?, telephone = ?,email = ?");
            $password = password_hash($_POST['password'],PASSWORD_BCRYPT);
            $req->execute([$_POST['username'],$password,$_POST['som'],$_POST['nom'],$_POST['prenom'],$_POST['phone'],$_POST['email']]);
            session_start();
            $_SESSION['flash']['success']= 'Votre compte a été créé avec success';
            header('Location:login.php');
            exit();
        }
    }
?>
<?php require  '../inc/header.php'; ?>
<h1>S'inscrire</h1>


<?php if(!empty($errors)): ?>
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <ul>
            <?php foreach($errors as $i): ?>
                <li><?= $i; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<form action="" method="post">


    <div class="">
        <div class="form-group">
            <label for="">Nom</label>
            <input type="text" name="nom" class="form-control" autocomplete="on"  placeholder="nom"
                <?php
                if (!empty($errors) && isset($_POST["nom"]) && !isset($errors['nom'])){
                    echo "value=".$_POST['nom'];
                }
                ?>>
        </div>
        <div class="form-group">
            <label for="">Prénom</label>
            <input type="text" name="prenom" class="form-control" autocomplete="on"  placeholder="Prénom"
                <?php
                if (!empty($errors) && isset($_POST["prenom"]) && !isset($errors['prenom'])){
                    echo "value=".$_POST['prenom'];
                }
                ?>>
        </div>
        <div class="form-group">
            <label for="">Pseudo</label>
            <input type="text" name="username" class="form-control" autocomplete="on" placeholder="monsieurX"
                <?php
                if (!empty($errors) && isset($_POST["username"]) && !isset($errors['username'])){
                    echo "value=".$_POST['username'];
                }
                ?>>
        </div>
        <div class="form-group">
            <label for="" id="">SOM</label>
            <input type="number" name="som" class="form-control" autocomplete="on" placeholder="1234567"
                <?php
                if (!empty($errors) && isset($_POST["som"]) && !isset($errors['som'])){
                    echo "value=".$_POST['som'];
                }
                ?>>
        </div>

        <div class="form-group">
            <label for="">Téléphone</label>
            <input type="tel" name="phone" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" autocomplete="on" placeholder="0600000000 (ou +212600000000)"
                <?php
                if (!empty($errors) && isset($_POST["phone"]) && !isset($errors['phone'])){
                    echo "value=".$_POST['phone'];
                }
                ?>>
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="email" name="email" class="form-control" autocomplete="on" placeholder="example@mail.com"
                <?php
                if (!empty($errors) && isset($_POST["email"]) && !isset($errors['email'])){
                    echo "value=".$_POST['email'];
                }
                ?>>
        </div>

        <div class="form-group">
            <label for="">Mot de passe</label>
            <input type="password" name="password" class="form-control" />
        </div>

        <div class="form-group">
            <label for="">Confirmer mot de passe</label>
            <input type="password" name="password_confirm" class="form-control" />
        </div>

        <button type="submit" class="btn btn-default">M'inscrire</button>
    </div>
</form>



<?php require  '../inc/footer.php'; ?>

